﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgpTimeTableLib
{
    public class ChannelDto
    {
        public int VirtualWeekIndex { get; private set; }
        public string VirtualWeekName { get; private set; }
        public int VirtualStartHour { get; private set; }
        public int RealWeekIndex { get; private set; }
        public string RealWeekName { get; private set; }
        public int RealStartHour { get; private set; }
        public int StartMin { get; private set; }
        public int MinLength { get; private set; }
        public string Url { get; private set; }
        public string MailAddress { get; private set; }
        public string RP { get; private set; }
        public string Title { get; private set; }
        public string SrcRP { get; private set; }

        private class ChannelRefDto
        {
            public bool Top { get; set; }
            public ChannelDto Channel { get; set; }
        }

        public static ChannelDto[][] Read(Stream stream)
        {
            string html;
            using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
            {
                html = reader.ReadToEnd();
            }
            return Read(html);
        }

        public static ChannelDto[][] Read(string html)
        {
            string strHtml = html;
            int weekMax = 7;
            int timeMax = (24 * 60);
            string[] weekTbl;
            ChannelRefDto[,] chTbl = new ChannelRefDto[weekMax, timeMax];
            ChannelDto[][] retDtos;
            Func<string, string, string> removeOnce = (string str, string removeStr) =>
            {
                if (removeStr == null)
                {
                    return str;
                }
                else
                {
                    int index = str.IndexOf(removeStr);
                    if (index < 0)
                    {
                        return str;
                    }
                    else
                    {
                        string str0 = str.Substring(0, index);
                        string str1 = str.Substring(index + removeStr.Length);
                        return str0 + str1;
                    }
                }
            };
            Func<string, string, string, string, bool, string> subTagBlock = (string str, string def, string strStart, string strEnd, bool isTrim) =>
            {
                int startIndex, endIndex, length;
                string ret = null;
                startIndex = str.IndexOf(strStart);
                if (startIndex >= 0)
                {
                    endIndex = str.IndexOf(strEnd, startIndex + strStart.Length);
                    if (endIndex >= 0)
                    {
                        length = (endIndex - startIndex) + strEnd.Length;
                        ret = str.Substring(startIndex, length);
                        if (isTrim)
                        {
                            ret = removeOnce(ret, strStart);
                            ret = removeOnce(ret, strEnd);
                        }
                        return ret;
                    }
                }
                return def;
            };
            Func<string, string, string, string> subTagBlock2 = (string str, string strStart, string strEnd) =>
            {
                int iStart = str.IndexOf(strStart);
                int iEnd = str.IndexOf(strEnd);
                if (iEnd < 0)
                {
                    return null;
                }
                else
                {
                    int length;
                    string ret;
                    iEnd += strEnd.Length;
                    if ((iStart < 0) || (iStart > iEnd))
                    {
                        iStart = 0;
                    }
                    length = iEnd - iStart;
                    ret = str.Substring(iStart, length);
                    return ret;
                }
            };
            //init
            {
                //delete comment
                {
                    string comment;
                    while (!string.IsNullOrEmpty(comment = subTagBlock(strHtml, null, "<!--", "-->", false)))
                    {
                        strHtml = removeOnce(strHtml, comment);
                    }
                }
                //seek
                {
                    strHtml = strHtml.Substring(strHtml.IndexOf("<div class=\"header-play\">"));
                    strHtml = strHtml.Substring(strHtml.IndexOf("<table class=\"timetb-ag\""));
                }
            }

            //read
            {
                //thead
                {
                    string strThead = subTagBlock(strHtml, null, "<thead", "</thead>", false);
                    string strTr = subTagBlock(strHtml, null, "<tr", "</tr>", false);
                    Func<string, string[]> collectTd = (string str) =>
                    {
                        List<string> weekList = new List<string>();
                        while (true)
                        {
                            string s = subTagBlock2(str, "<td", "</td>");
                            if (string.IsNullOrEmpty(s))
                            {
                                break;
                            }
                            else
                            {
                                str = removeOnce(str, s);
                                s = removeOnce(s, subTagBlock(s, null, "<td", ">", false));
                                s = removeOnce(s, subTagBlock(s, null, "</td>", "", false));
                                s = removeOnce(s, subTagBlock(s, null, "曜日", "", false));
                                s = subTagBlock(s, s, "(", ")", true);
                                s = subTagBlock(s, s, "（", "）", true);

                                weekList.Add(s);
                            }
                        };
                        return weekList.ToArray();
                    };
                    weekTbl = collectTd(strTr);
                }

                //tbody
                {
                    string strTbody = subTagBlock(strHtml, null, "<tbody", "</tbody>", false);
                    //trtd
                    {
                        Action<string> collectTr = null;
                        Action<string, int> collectTd = null;
                        Action<string, int> collectCh = null;
                        collectTr = (string str) =>
                        {
                            int row = 0;
                            while (true)
                            {
                                string s = subTagBlock2(str, "<tr", "</tr>");
                                if (string.IsNullOrEmpty(s))
                                {
                                    break;
                                }
                                else
                                {
                                    str = removeOnce(str, s);
                                    collectTd(s, row);
                                }
                                row++;
                            };
                        };

                        collectTd = (string str, int row) =>
                        {

                            while (true)
                            {
                                string s = subTagBlock2(str, "<td", "</td>");
                                if (string.IsNullOrEmpty(s))
                                {
                                    break;
                                }
                                else
                                {
                                    str = removeOnce(str, s);
                                    collectCh(s, row);
                                }
                            };
                        };

                        collectCh = (string str, int row) =>
                        {
                            int rowspan = 1;
                            int virtualWeekIndex;
                            string th = subTagBlock(str, null, "<td ", ">", true);
                            string divTime = str.Substring(str.IndexOf("<div class=\"time\""));
                            string divTitle = str.Substring(str.IndexOf("<div class=\"title-p\""));
                            string divRp = str.Substring(str.IndexOf("<div class=\"rp\""));
                            divTime = subTagBlock(divTime, null, ">", "</div>", true);
                            divTitle = subTagBlock(divTitle, null, ">", "</div>", true);
                            divRp = subTagBlock(divRp, null, ">", "</div>", true);
                            ChannelDto ch = new ChannelDto();
                            //week
                            {
                                int i = 0;
                                for (; i < weekMax; i++)
                                {
                                    if (chTbl[i, row] == null)
                                    {
                                        break;
                                    }
                                }
                                virtualWeekIndex = i;
                            }
                            //rownum
                            {
                                string tmp = th;
                                string strRowspan = subTagBlock(str, null, "rowspan=\"", "\"", true);
                                if (!string.IsNullOrEmpty(strRowspan))
                                {
                                    rowspan = int.Parse(strRowspan);
                                }
                                ch.MinLength = rowspan * 1;
                            }
                            //time
                            {
                                string tmp = divTime;
                                string span = subTagBlock(tmp, null, "<span>", "</span>", false);
                                string[] times;
                                int hour = 0;
                                int minute = 0;
                                if (!string.IsNullOrEmpty(span))
                                {
                                    tmp = removeOnce(tmp, span);
                                }
                                tmp = tmp.Trim();
                                tmp = tmp.Substring(0, 5);
                                times = tmp.Split(':');
                                if (times != null)
                                {
                                    if (times.Length > 0)
                                    {
                                        hour = int.Parse(times[0]);
                                    }
                                    if (times.Length > 1)
                                    {
                                        minute = int.Parse(times[1]);
                                    }
                                }
                                ch.RealStartHour = hour;
                                ch.StartMin = minute;
                                ch.VirtualWeekIndex = virtualWeekIndex;
                                ch.VirtualWeekName = weekTbl[virtualWeekIndex];
                            }
                            //title
                            {
                                string tmp = divTitle;
                                string a = subTagBlock(tmp, null, "<a href=\"", ">", false);
                                if (!string.IsNullOrEmpty(a))
                                {
                                    tmp = removeOnce(tmp, a);
                                    tmp = removeOnce(tmp, "</a>");
                                }
                                tmp = tmp.Trim();
                                ch.Title = tmp;
                            }
                            //SrcRP
                            {
                                string tmp = divRp;
                                tmp = tmp.Trim();
                                ch.SrcRP = tmp;
                            }
                            //url
                            {
                                string tmp = divTitle;
                                string a = subTagBlock(tmp, null, "<a href=\"", "\"", false);
                                if (!string.IsNullOrEmpty(a))
                                {
                                    string url = subTagBlock(tmp, null, "<a href=\"", "\"", true);
                                    url = url.Trim();
                                    ch.Url = url;
                                }
                            }
                            //mailto
                            {
                                string tmp = divRp;
                                string a = subTagBlock(tmp, null, "<a href=\"mailto:", "\"", false);
                                if (!string.IsNullOrEmpty(a))
                                {
                                    string mailAddress = subTagBlock(a, null, "<a href=\"mailto:", "\"", true);
                                    mailAddress = mailAddress.Trim();
                                    ch.MailAddress = mailAddress;
                                }
                            }
                            //RP
                            {
                                string tmp = divRp;
                                string a = subTagBlock(tmp, null, "<a href=\"", "</a>", false);
                                if (!string.IsNullOrEmpty(a))
                                {
                                    tmp = removeOnce(tmp, a);
                                }
                                tmp = tmp.Trim();
                                ch.RP = tmp;
                            }

                            for (int i = 0; i < rowspan; i++)
                            {
                                int j = row + i;
                                ChannelRefDto prgRef = new ChannelRefDto() { Top = (i == 0), Channel = ch };
                                chTbl[virtualWeekIndex, j] = prgRef;
                            }
                        };
                        collectTr(strTbody);
                    }
                }
            }
            //normalize
            {
                for (int j = 0; j < chTbl.GetLength(0); j++)
                {
                    int last = 0;
                    for (int i = 0; i < chTbl.GetLength(1); i++)
                    {
                        ChannelRefDto dto = chTbl[j, i];
                        if (dto != null)
                        {
                            if (dto.Top)
                            {
                                ChannelDto ch = dto.Channel;
                                int virtualStartHour = ch.RealStartHour;
                                int realWeekIndex = ch.VirtualWeekIndex;
                                while (virtualStartHour < last)
                                {
                                    virtualStartHour += 24;
                                    realWeekIndex += 1;
                                    realWeekIndex %= weekTbl.Length;
                                }
                                ch.VirtualStartHour = virtualStartHour;
                                ch.RealWeekIndex = realWeekIndex;
                                ch.RealWeekName = weekTbl[realWeekIndex];
                                last = virtualStartHour;
                            }
                        }
                    }
                }
            }
            //rebuild
            {
                List<List<ChannelDto>> allList = new List<List<ChannelDto>>();
                for (int j = 0; j < chTbl.GetLength(0); j++)
                {
                    List<ChannelDto> list = new List<ChannelDto>();
                    for (int i = 0; i < chTbl.GetLength(1); i++)
                    {
                        ChannelRefDto dto = chTbl[j, i];
                        if (dto != null)
                        {
                            if (dto.Top)
                            {
                                list.Add(dto.Channel);
                            }
                        }
                    }
                    allList.Add(list);
                }
                retDtos = new ChannelDto[allList.Count][];
                for (int i = 0; i < retDtos.Length; i++)
                {
                    retDtos[i] = allList[i].ToArray();
                }
            }
            return retDtos;
        }
    }
}
