# Q. 番組表をスクレイピングしたいです。

# A. 犬を飼うといいですよ



## !!!注意!!! ###

適当にスクレイピングしているので

番組が抜け落ちてても責任取りません。

誰かから怒られても如何なる責任は取りません。


## これはwhat's？ ###

番組表スクレイピング用ライブラリです。

出力機能は設けていないので

勝手にフロント エンドで対応してください。


## 環境 ###
・Visual Studio Express 2013 for Windows Desktop

・.NET Framework 4.5

・C#


## 使い方 ###

サンプルから読み解いてください。

[AgpTimeTable](https://bitbucket.org/libraplanet/agptimetable/overview)